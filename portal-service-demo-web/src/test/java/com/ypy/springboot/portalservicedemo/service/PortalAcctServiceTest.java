package com.ypy.springboot.portalservicedemo.service;

import com.myclass.response.AcctInfoResponse;
import com.ypy.springboot.portalservicedemo.PortalServiceDemoApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PortalServiceDemoApplication.class)
@WebAppConfiguration
public class PortalAcctServiceTest {

    @Autowired
    private PortalAcctServiceImpl portalAcctService;

    @Test
    public void testQueryAcctInfos(){

        List<AcctInfoResponse> responses = portalAcctService.queryAcctInfos();

        System.out.println(responses);

    }

}
