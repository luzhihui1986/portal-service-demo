package com.ypy.springboot.portalservicedemo.service;

import com.myclass.response.AcctInfoResponse;
import com.myclass.service.AcctService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PortalAcctServiceImpl {

    @Autowired
    private AcctService acctServiceImpl;

    public List<AcctInfoResponse> queryAcctInfos(){
        return acctServiceImpl.queryAcctInfos();
    }

}
